/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  Chiffrement de messages                                        *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Killian Falguiere                                            *
*                                                                             *
*  Nom-prénom2 : Vincent Fernandez                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  main.c                                                   *
*                                                                             *
******************************************************************************/

#include <string.h>
#include <locale.h>
#include "chiffrement.h"
#include "fichier.h"

/**
 * Vérifie:
 *      => quand ch est vraie: si resp est égual à 'o' ou 'O' ou 'n' ou 'N'
 *      => quand ch est faux: si resp est égual à 1 ou 2 ou 3
 *
 * @param resp
 * @param ch
 * @return 0 si resp est valide sinon 1
 */
static int isValidEncryptor(int resp, bool ch);

/**
 * Vérifie que la chaine de caractère val ne contient pas de caractères en majuscule
 *
 * @param val
 * @return 0 si val est valide sinon 1
 */
static int isTiny(wchar_t *val);

/*
 * BODY
 */

void main() {
    setlocale(LC_ALL, "fr_FR.utf8");
    wprintf(L"Vous pouvez utiliser 2 algorithmes de chiffrement:\n1. Chiffrement de Cesar \n2. Chiffrement de Vigenere\n3. Fermer le programme\n");

    /*
     * Choix du chiffrement
     */
    wint_t resp;
    do {
        wscanf(L" %d", &resp);
        getwchar(); // evite la boucle infinie
    } while (isValidEncryptor(resp, false) == EXIT_FAILURE);
    if(resp == 3) {
        return;
    }
    wprintf(L"Vous avez selectionne le Chiffrement de %ls", resp == 1 ? L"Cesar" : L"Vigenere");
    wprintf(L"\nVoulez vous :\n1. Chiffrer un message\n2. Dechiffrer un message\n3. Fermer le programme\n");

    wint_t val;
    do {
        wscanf(L" %d", &val);
        getwchar(); // evite la boucle infinie
    } while (isValidEncryptor(val, false) == EXIT_FAILURE);
    if(val == 3) {
        return;
    }

    /*
     * Récupération du message
     */
    wprintf(L"Entrez votre message%ls:\n", resp == 2 ? L"(en minuscules)" : L"");
    Text text;
    do {
        getTextInTerminal(&text);
        convert(&text);
    } while (isContentsValid(text) == EXIT_FAILURE || (resp == 2 ? isTiny(text.content) == EXIT_FAILURE : false));
    wprintf(L"Votre message est: %ls (Taille: %d)\n", text.content, text.size);

    if(resp == 1) { // Cesar

        /*
         * Récupération de la clé
         */
        wprintf(L"Entrez votre cle(Nombre de decalage):\n");
        wint_t key;
        wscanf(L" %d", &key);

        if(val == 1) { // chiffrement
            if(cesar(&text, key, false) == EXIT_FAILURE) {
                fwprintf(stderr, L"Erreur pendant le chiffrement du message...");
                return;
            }
            wprintf(L"Votre message une fois chiffre est: %ls\n", text.content);
        } else if(val == 2) { // dechiffrement
            if(cesar(&text, key, true) == EXIT_FAILURE) {
                fwprintf(stderr, L"Erreur pendant le dechiffrement du message...");
                return;
            }
            wprintf(L"Votre message une fois dechiffre est: %ls\n", text.content);
        }
    } else if(resp == 2) { // Vigenere

        /*
         * Récupération de la clé
         */
        wprintf(L"Entrez votre cle(en minuscules):\n");
        wchar_t key[500];
        do {
            //wscanf(L" %s", key);
            wRead(key, 500);
        } while (isTiny(key));

        if(val == 1) { // chiffrement
            if(vigenere(&text, key, false) == EXIT_FAILURE) {
                fwprintf(stderr, L"Erreur pendant le chiffrement du message...");
                return;
            }
            wprintf(L"Votre message une fois chiffre est: %ls\n", text.content);
        } else if(val == 2) { // dechiffrement
            if(vigenere(&text, key, true) == EXIT_FAILURE) {
                fwprintf(stderr, L"Erreur pendant le dechiffrement du message...");
                return;
            }
            wprintf(L"Votre message une fois dechiffre est: %ls\n", text.content);
        }
    }

    /*
     * Sauvegarde du résultat dans un fichier
     */
    wchar_t cr[2];
    wprintf(L"\nVoulez vous sauvegarder ce resultat dans un fichier? (O/N)\n");
    do {
        wRead(cr, 2);
    } while (isValidEncryptor(cr[0], true) == EXIT_FAILURE);

    if(cr[0] == L'O' || cr[0] == L'o') {
        if (addResult(text.content) == EXIT_FAILURE) {
            fwprintf(stderr, L"Erreur pendant la sauvegarde...\n");
        } else {
            fwprintf(stderr, L"Resultat sauvegarde...\n");
        }
    }

    /*
     * Code récursif pour entrer un nouveau message si il le faut
     */
    wprintf(L"\nVoulez vous entrer un nouveau message ? (O/N)\n");
    do {
        wRead(cr, 2);
    } while (isValidEncryptor(cr[0], true) == EXIT_FAILURE);
    free(text.content);
    if(cr[0] == L'O' || cr[0] == L'o') {
        main();
    }
}


static int isValidEncryptor(int resp, bool ch) {
    if(ch ? (resp == 79 || resp == 78 || resp == 110 || resp == 111) : (resp == 1 || resp == 2 || resp == 3)) {
        return EXIT_SUCCESS;
    }
    if(resp == 0) return EXIT_FAILURE;
    fwprintf(stderr, L"Votre reponse n'est pas valide !");
    return EXIT_FAILURE;
}

static int isTiny(wchar_t *val) {
    for(int i = 0; val[i] != '\0'; i++) {
        if(val[i] < 97 || val[i] > 122) {
            fwprintf(stderr, L"Votre reponse n'est pas valide !");
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}