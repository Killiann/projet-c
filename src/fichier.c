/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  Chiffrement de messages                                        *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Killian Falguiere                                            *
*                                                                             *
*  Nom-prénom2 : Vincent Fernandez                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  fichier.c                                                *
*                                                                             *
******************************************************************************/
#include "fichier.h"

/**
 * Convertie un wchar_t* en char*
 * Source: https://stackoverflow.com/questions/3019977/convert-wchar-t-to-char/55715607
 *
 * @param pwchar
 * @return le resultat de la conversion
 */
static char* wchar_to_char(const wchar_t* pwchar);

/*
 * BODY
 */

int addResult(wchar_t *result) {
    FILE *file = NULL;
    file = fopen("resultats.txt", "a" );
    if(file == NULL) {
        return EXIT_FAILURE;
    }

    fprintf(file, "Resultat: ");
    fprintf(file, wchar_to_char(result));
    fprintf(file, "\n");

    fclose(file);
    return EXIT_SUCCESS;
}

static char* wchar_to_char(const wchar_t* pwchar)
{
    // get the number of characters in the string.
    int currentCharIndex = 0;
    char currentChar = pwchar[currentCharIndex];

    while (currentChar != '\0')
    {
        currentCharIndex++;
        currentChar = pwchar[currentCharIndex];
    }

    const int charCount = currentCharIndex + 1;

    // allocate a new block of memory size char (1 byte) instead of wide char (2 bytes)
    char* filePathC = (char*)malloc(sizeof(char) * charCount);

    for (int i = 0; i < charCount; i++)
    {
        // convert to char (1 byte)
        char character = pwchar[i];

        *filePathC = character;

        filePathC += sizeof(char);

    }
    filePathC += '\0';

    filePathC -= (sizeof(char) * charCount);

    return filePathC;
}