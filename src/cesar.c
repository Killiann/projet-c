/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  Chiffrement de messages                                        *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Killian Falguiere                                            *
*                                                                             *
*  Nom-prénom2 : Vincent Fernandez                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  cesarc                                                   *
*                                                                             *
******************************************************************************/

#include "chiffrement.h"

/**
 * Permet d'avoir le nombre et la première lettre majuscule/minuscule et des chiffres
 *
 * @param ascii
 * @param var
 * @return 0 si le caractère peut être chiffré sinon 1
 */
static int caractere(wchar_t ascii, wchar_t var[3]);

/*
 * BODY
 */

int cesar(Text *text, int key, bool decrypt) {
    if(isContentsValid(*text) == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    for(int i = 0; i < text->size; i++) {
        wchar_t var[3];
        wchar_t c = text->content[i];

        if(c == ' ' || caractere(c, var) == EXIT_FAILURE) {
            continue;
        }

        if(decrypt) {
            text->content[i] = ((c - var[1] + var[0] + key) % 26) + var[1];
        } else {
            text->content[i] = ((c - var[1] + var[0] - key) % 26) + var[1];
        }
    }

    return EXIT_SUCCESS;
}

static int caractere(wchar_t ascii, wchar_t var[3]) {
    if (ascii >= 65 && ascii <= 96) { // Si c'est une lettre majuscule
        var[0] = 26; // Nombre de lettres majuscules
        var[1] = 65; // Premiere lettre majuscule
    } else if (ascii >= 97 && ascii <= 122) { // Si c'est une lettre minuscule
        var[0] = 26; // Nombre de lettres minuscule
        var[1] = 97; // Premiere lettre minuscule
    } else if (ascii >= 48 && ascii <= 57){ // Si c'est un chiffre
        var[0] = 9; // Nombre de chiffres
        var[1] = 48; // Premier chiffre
    } else if (ascii != 32){
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
