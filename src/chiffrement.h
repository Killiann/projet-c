/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  Chiffrement de messages                                        *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Killian Falguiere                                            *
*                                                                             *
*  Nom-prénom2 : Vincent Fernandez                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  chiffrement.h                                            *
*                                                                             *
******************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include <wchar.h>

#define TAILLE_MAX 1024

struct Text {
    wchar_t *content;
    size_t size;
};
typedef struct Text Text;

/**
 * Vérifie qu'il n'y a pas de caractères interdit dans une struct Text
 *
 * @param text
 * @return 0 si la struct Text est valide sinon 1
 */
int isContentsValid(Text text);

/**
 * Convertis les accents en la lettre sans l'accent pour éviter les problèmes avec les
 * differents algorithmes de chiffrement. Il modifie text
 *
 * @param text
 */
void convert(Text *text);

/**
 * Récupère ce que l'utilisateur a écrit dans le terminal. Il le stocke dans text
 *
 * @param text
 */
void getTextInTerminal(Text *text);

/**
 * Utilise l'algorithme de chiffrement cesar pour chiffrer/déchiffrer(en fonction de decrypt) text avec la key
 *
 * @param text
 * @param key
 * @param decrypt
 * @return 0 si il n'y a eu aucun problème pendant le chiffrement sinon 1
 */
int cesar(Text *text, int key, bool decrypt);

/**
 * Utilise l'algorithme de chiffrement de vigenere pour chiffrer/déchiffrer(en fonction de decrypt) text avec la key
 *
 * @param text
 * @param key
 * @param decrypt
 * @return 0 si il n'y a eu aucun problème pendant le chiffrement sinon 1
 */
int vigenere(Text *text, wchar_t *key, bool decrypt);

/**
 * Récupère le texte écrit dans le terminale d'une size maximale et le stocke dans text
 *
 * @param text
 * @param size
 * @return 0 si il n'y a eu aucun problème sinon 1
 */
int wRead(wchar_t *text, int size);