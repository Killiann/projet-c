/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  Chiffrement de messages                                        *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Killian Falguiere                                            *
*                                                                             *
*  Nom-prénom2 : Vincent Fernandez                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  gestionTexte.c                                           *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "chiffrement.h"

/*
 * BODY
 */

void getTextInTerminal(Text *text) {
    text->content = calloc(TAILLE_MAX, sizeof(wchar_t));
    wRead(text->content, TAILLE_MAX);
    text->size = wcslen(text->content);
}

int isContentsValid(Text text) {
    for(int i = 0; i < text.size; i++) {
        wchar_t c = text.content[i];
        if((c < 48 && c != 32) || (c > 57 && c < 65) || (c > 90 && c < 97) || c > 122) {
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

void convert(Text *text) {
    int accents[2][58] = {
            {192, 193, 194, 195, 196, 197, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,
             221, 224, 225, 226, 227, 228, 229, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 248, 249, 250, 251, 252, 253, 255},
            {65, 65, 65, 65, 65, 65, 67, 69, 69, 69, 69, 73, 73, 73, 73, 68, 78, 79, 79, 79, 79, 79, 120, 48, 85, 85, 85, 85, 89, 97, 97, 97, 97, 97, 97,
             99, 101, 101, 101, 101, 105, 105, 105, 105, 111, 110, 111, 111, 111, 111, 111, 48, 117, 117, 117, 117, 121, 121}
    };

    for(int i = 0; i < text->size; i++) {
        for(int u = 0; u < 58; u++) {
            if(text->content[i] == accents[0][u]) {
                text->content[i] = accents[1][u];
            }
        }
    }
}

void wviderBuffer() {
    wint_t c = 0;
    while (c != '\n' && c != WEOF) {
        c = getwchar();
    }
}

int wRead(wchar_t *text, int size) {
    wchar_t *p = NULL;

    if (fgetws(text, size, stdin) != NULL) {
        p = wcschr(text, '\n');

        if (p != NULL) {
            *p = '\0';
        } else {
            wviderBuffer();
        }

        return EXIT_SUCCESS;
    } else {
        wviderBuffer();
        return EXIT_FAILURE;
    }
}
