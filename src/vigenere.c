/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  Chiffrement de messages                                        *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Killian Falguiere                                            *
*                                                                             *
*  Nom-prénom2 : Vincent Fernandez                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  vigenere.c                                                   *
*                                                                             *
******************************************************************************/

#include <string.h>
#include "chiffrement.h"

/**
 * Chiffre text en fonction de key et de sa taille size
 *
 * @param text
 * @param key
 * @param size
 */
static void encrypt(Text *text, wchar_t *key, size_t size);

/**
 * Déchiffre key et stocke le résultat dans keyT
 *
 * @param key
 * @param keyT
 */
static void decryptVigenere(wchar_t *key, wchar_t *keyT);

/*
 * BODY
 */

int vigenere(Text *text, wchar_t *key, bool decrypt) {
    if(isContentsValid(*text) == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    size_t size = wcslen(key);
    if(!decrypt) {
        encrypt(text, key, size);
    } else {
        wchar_t keyT[size + 1];
        decryptVigenere(key, keyT);
        encrypt(text, keyT, size);
    }
}

static void encrypt(Text *text, wchar_t *key, size_t size) {
    for(int i = 0; i < text->size; i++) {
        wchar_t c = text->content[i];


        if(c >= L'a' && c <= L'z') {
            text->content[i] = L'a' + ((c + key[i % size] - 2 * L'a') % 26);
        } else if(c >= L'A' && c <= L'Z') {
            text->content[i] = L'A' + ((c + key[i % size] - L'a' * L'A') % 26);
        }
    }
}

static void decryptVigenere(wchar_t *key, wchar_t *keyT) {
    int i;
    for(i = 0; key[i] != 0; i++){
        keyT[i] = ((26 - (key[i] - L'a')) % 26) + L'a';
    }
    keyT[i] = L'\0';
}