# Projet 3 : Chiffrement de messages

[![Version](https://img.shields.io/badge/Langage-C-blue)]()

Ce programme contient 2 algorithmes de chiffrement:
- Cesar
- Vigenere

![Banner](https://i.gyazo.com/237ffb6de2c85d1d77d0f1b6ef972d93.png)

## Fonctions

| Nom | Description |
| ------ | ------ |
| isContentsValid | Vérifie qu'il n'y a pas de caractères interdit |
| convert | Convertis les accents en la lettre sans l'accent |
| getTextInTerminal | Récupère ce que l'utilisateur a écrit dans le terminal et le stocke dans une structure |
| cesar | Utilise l'algorithme de chiffrement cesar pour chiffrer/déchiffrer un texte |
| vigenere | Utilise l'algorithme de chiffrement de vigenere pour chiffrer/déchiffrer un texte |
| wRead | Récupère le texte écrit dans le terminale |
| addResult | Rajoute la chaine de caractère dans le fichier des résultats |

#### isContentsValid
```c
int isContentsValid(Text text)
```
> Vérifie qu'il n'y a pas de caractères interdit dans une struct Text
> Retourne 0 si la struct Text est valide sinon 1

#### convert
```c
void convert(Text *text)
```

> Convertis les accents en la lettre sans l'accent pour éviter les problèmes avec les differents algorithmes de chiffrement. Il modifie text

#### getTextInTerminal
```c
void getTextInTerminal(Text *text)
```

> Récupère ce que l'utilisateur a écrit dans le terminal. Il le stocke dans text

#### cesar
```c
int cesar(Text *text, int key, bool decrypt)
```

> Utilise l'algorithme de chiffrement cesar pour chiffrer/déchiffrer(en fonction de decrypt) text avec la key
> Retourne 0 si il n'y a eu aucun problème pendant le chiffrement sinon 1

#### vigenere
```c
int vigenere(Text *text, wchar_t *key, bool decrypt)
```

> Utilise l'algorithme de chiffrement de vigenere pour chiffrer/déchiffrer(en fonction de decrypt) text avec la key
> Retourne 0 si il n'y a eu aucun problème pendant le chiffrement sinon 1

#### wRead
```c
int wRead(wchar_t *text, int size)
```

> Récupère le texte écrit dans le terminale d'une size maximale et le stocke dans text
> Retourne 0 si il n'y a eu aucun problème sinon 1

#### addResult
```c
int addResult(wchar_t *result)
```

> Rajoute la chaine de caractère result dans le fichier "resultat.txt"
> Retourne 0 si il n'y a eu aucun problème avec l'ouverture du fichier sinon 1

## Installation
Pour compiler le programme il faut le compilateur [GCC](https://doc.ubuntu-fr.org/gcc).

Pour lancer le programme il faut faire:
```sh
git clone https://framagit.org/Killiann/projet-c.git
make
./main
```